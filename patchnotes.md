# 18 February 2023 - prospective patch noes

## Designer's Notes
Reminder: we now use mpq autogeneration, and have an updater, all courtesy of Veeq7! Get the updater [here](https://mega.nz/folder/r5QXXL6I#eXy7ptVhsa_wJd-AHuPyEg)!

To use the updater, delete your previous CMBW installations and place the updater in an empty folder. Running it will prompt you to select which version of the mod to download, and a (usually lengthy) process of downloading the entire repository will begin.

Once the launcher has finished downloading the initial installation, using it to grab individual updates will be much faster, and much more automatic!

Launch times will be a bit slower. This will be rectified later.

Aside from that: We will continue smoothing out early-game unit roles and adding vital audiovisuals.

## Gameplay
- Melee air units now have slightly improved chasing capabilities (c. DarkenedFantasies)
- A randomized lard timer on going from "stop" order to "guard" order has been removed (c. Veeq7)

## Bugfixes
- Crashes with improper tooltips have been resolved
- Gosvileth no longer crashes when carrying resources
- Incomplete Wyverns no longer attack from inside the Starport
- Irradiate no longer procs EMP (legacy feature finally removed)
- Strange buttonset-related bugs with Terran production have been resolved

## Audiovisuals
- Superior beta graphics for the Harakan flamethrower attack and the Seraph turret have been added
- Sealed holes:
	- Dracadin
	- Spraith Colony
- New audiovisuals:
	- Madrigal (Dirge proc)
- New unit responses:
	- Phobos

## UI
- Dashed selection circles now show in replays to indicate player selection
- Order lines now always show in witness mode or in replays
- Witness selections are no longer shown in live games

## Terran
- [Cyprian](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/cyprian):
	- Time cost now 20, from 18
	- HP now 60, from 50
	- Weapon damage now 10, from 8
- [Spider Mine](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/spider-mine):
	- No longer triggers against hidden units (unless they are detected/revealed)
- [Matador](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/matador):
	- Armor now 1, from 0

## Protoss
- [Scribe](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/scribe):
	- HP now 30, from 25
	- Shields now 15, from 20
- [Dracadin](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/dracadin):
	- Armor penetration now 2, from 3
- [Simulacrum](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/simulacrum):
	- Mineral and time costs now 125/22, from 75/20
	- Armor penetration now 1, from 2
	- Now provides two per requisition
- [Manifold](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/manifold):
	- Mineral cost now 100, from 75
	- Armor now 1, from 2

## Zerg
- [Lakizilisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/lakizilisk):
	- No longer revealed upon attacking
	- No longer gains armor while burrowed
	- *Now that burrowed units have less sight range and are revealed when ground units move near to them, the Lakizilisk no longer feels disproportionately powerful by being a hidden attacker.*
- **NEW:** [Spirtith Colony](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/spirtith-colony):
	- Advanced anti-air defensive cell with a bouncing attack
	- Mutated from Spraith Colony; requires Irol Iris or greater