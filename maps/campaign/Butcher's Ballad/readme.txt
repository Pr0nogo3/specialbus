I. OVERVIEW

	Current Version: 1.2
	
	Author: knightoftherealm
	Voice Acting: Pr0nogo, IskatuMesk
	Testing: Connor5620, Keyan


	Butcher's Ballad is a custom campaign that takes place in the native StarCraft setting.
	It uses the "Cosmonarchy-BW" game state, which requires version 1.16.1 of StarCraft.
	It is not vanilla compatible. Having basic knowledge of the new tech tree is recommended.

	The first act of this story follows the brutal Omega Squadron and its officers during
	their campaign to quell the rebellion against the Dominion on Sigmaris Prime.

	1. You can download StarCraft 1.61.1 here:   https://mega.nz/folder/ulwTzYRa#J4rlZkgEn_HU1nPCTw2wPg

	2. Installation Guide:   https://www.fraudsclub.com/starcraft/play/

	3. Info about the "Cosmonarchy-BW" game state:   https://www.fraudsclub.com/cosmonarchy-bw/sites/


	*Note that Cosmonarchy-BW is still under active development


II. CONTENT

  - ACT I, PART I: "Clenched Fist"
	
	Commander Colt and Lieutenant Tobias take Omega Squadron to Sigmaris Prime and
	prepare to assail a small rebel company tasked with establishing an outpost in
	the wilderness. They must be destroyed quickly, so that Dominion's forces can proceed
	to the regional capital, Santiago Nova.

	Map Size: 128x128
	Tileset: Jungle World

	Allies: None
	Enemies: Brass Company


  - ACT I, PART II: "Ad Portas"
	
	A larger and better equipped wing of this rebel company guards the way to Santiago Nova.
	Their fate is resigned.

	Map Size: 128x128
	Tileset: Badlands

	Allies: None
	Enemies: Brass Company


  - ACT I, PART III: "Alpha Prey"
	
	An Alpha Squadron contingent that was to reinforce Omega Squadron has crash-landed
	in a nearby mountain range and stirred up the slumbering zerg hive clusers in that area.
	They must be assisted if they are to survive the onslaught.

	Map Size: 192x192
	Tileset: Ice World

	Allies: Alpha Squadron
	Enemies: Frar Brood, Naefr Brood


  - ACT I, PART IV: "Unto Ruin"
	
	With their ally rescued, Omega Squadron moves to crush the remaining rebels
	and put an end to this rebellion.

	Map Size: 228x192
	Tileset: Jungle World

	Allies: Alpha Squadron
	Enemies: Farsight Company, Eden Collective, Ilska Brood


III. PATCH NOTES

	Version 1.2 (17/12/2022)

	 - Corrected several typos in briefings and in-game transmissions in Act I, Part I and IV
	 - Transmissions no longer ping random units of the same type as the talking unit
	 - Upon completing a map, the next map will now start automatically
	 - Rebalanced most vespene nodes in all Act I maps
	 - Rearranged several misplaced tiles and cliffs in Act I, Parts I and II
	 - Removed preplaced expansions for all players and reworked their starting assets in Act I, Part IV
	 - Increased the starting resources of all enemy AI players in Act I, Parts II and IV
	 - The AI have more sensible base layouts now in all Act I maps


	Version 1.1 (14/12/2022)

	 - Rebalanced mineral patches on all Act I maps, introducing patches with varying yields
	 - Added 2 more starting Masons to Brass Company in Act I, Part II
	 - Added 2 more starting Masons to Farsight company in Act I, Part IV
	 - Added 4 more starting Masons to Eden Collective in Act I, Part IV
	 - Removed some of the player's preplaced assets from Act I, Part IV
	 - Slightly increased starting mineral of force 2 in Act I, Part IV


	Version 1.0 (9/12/2022)

	 - Release version of Act I


IV. CONTACT

	If you wish to report bugs, you can reach out to me on "The No-Frauds Club"
	discord server hosted by Pr0nogo:   https://discord.com/invite/s5SKBmY